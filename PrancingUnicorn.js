var screenWidth = screen.width,
	screenHeight= screen.height,
    duration = 3000;

// Create the Unicorn and set on the page
function Unicorn() {
	var elem = document.createElement("img");
	elem.setAttribute("src", "https://media.giphy.com/media/WZmgVLMt7mp44/giphy.gif");
	elem.setAttribute("height", "200px");
	elem.setAttribute("width", "200px");
	elem.setAttribute("left", "-200px");
	elem.setAttribute("id", "unicorn");
	elem.setAttribute("style", "position: fixed; z-index: 1000; visibility: visible");	
	document.body.prepend(elem); 

    animateUnicorn(elem, duration);
}

// Move the Unicorn
function animateUnicorn(elem, dur) {
    var animation = elem.animate(
    	[{left: "-200px" }, 
    	 {left: screen.width+'px'}], 
    	{ duration: dur, 
    	fill: 'forwards'
    	}
    )
    animation.onfinish = function() {  // remove the Unicorn from the DOM
	    	return elem.remove();
    } 
}

// Event listener if U is pressed
addEventListener("keydown", function(event) {
if (event.keyCode == 85)
	  Unicorn();
});