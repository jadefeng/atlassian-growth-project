// Testing on https://www.atlassian.com/software/jira

// Add jQuery
var jq = document.createElement('script');
jq.src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js";
document.getElementsByTagName('head')[0].appendChild(jq);
jQuery.noConflict();

// Remove the Try it Free link and replace it with an email registration form
document.getElementsByClassName("button green-solid standard")[0].remove()
var formDiv = document.getElementsByClassName("product-tour-hero--video__button")[0]
var formString = '<form method="POST" id="gform" action="" onsubmit="return redirect()" > <h3> Get Started in 30 seconds </h3> <input type="email" id="emailField" name="email" placeholder="Your email" ><button type="submit" class="button green-solid standard">Try It Free</button></form>'
formDiv.insertAdjacentHTML( 'afterend', formString );

// Add CSS to the input form
var cssString = "box-shadow: inset 0 1px 3px #e3e3e3; border: 1px solid #ccc; border-radius: 6px; box-sizing: border-box; color: #333; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 16px; margin-bottom: 16px; outline: 0; padding: 0 8px; height: 36px; width: 100%"
document.getElementById("emailField").style.cssText = cssString

// Sending the results from the HTML form to a Google Form
	// Connected Google Form = https://docs.google.com/forms/d/1_L33cTogGUeZghQ3f-BGpFcR-Gih9rbbn-9EEX5A2I0/edit#responses
	// Analytics results = https://docs.google.com/forms/d/e/1FAIpQLSfwYPo2aHkkijbGmIkhpaaKLwSwaFcX6wjt9eqSdl8vW6RgDg/viewanalytics
jQuery(function($) {
	$('#gform').one('submit',function(){
	    var inputName = encodeURIComponent($('#emailField').val());
	    var baseURL = 'https://docs.google.com/forms/d/e/1FAIpQLSfwYPo2aHkkijbGmIkhpaaKLwSwaFcX6wjt9eqSdl8vW6RgDg/formResponse?entry.867681899=';
	    var submitRef = '&submit=-6634835355083423469';
	    var submitURL = (baseURL + inputName + submitRef); // the submit URL called to send the email address
	    // console.log(submitURL);
	    $.get(submitURL);
	})
});

// Drive the form submission to the old sign up flow
function redirect() {
  window.location.replace("https://atlassian.com/software/jira/try");
  return false;
}

