// https://www.atlassian.com/ondemand/signup/form?product=jira-software.ondemand,confluence.ondemand

// Add JQuery
var jq = document.createElement('script');
jq.src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js";
document.getElementsByTagName('head')[0].appendChild(jq);
jQuery.noConflict();

// hide the firstName and lastName divs
var firstNameDiv = document.getElementsByClassName("half left")[0];
var lastNameDiv = document.getElementsByClassName("half right")[0];
firstNameDiv.style.display = 'none'; // Unhash these when looking to debug
lastNameDiv.style.display = 'none';

// replace with a Full Name input
var splitCell = document.getElementsByClassName("split-cell")[0];
var inputStr = "<div class=\'right-side input-icon-container\'>  <input id=\'fullName\' tabindex=\'2\' onfocusout=\'fillInput()\' type=\'text\' name=\'fullName\' placeholder=\'Sally Smith\' ondemand-component=\'true\' class=\'ng-pristine ng-valid ng-valid-pattern ng-touched\' style=\'\'> </div>";
splitCell.insertAdjacentHTML( 'afterend', inputStr );

// Function to split the name into first and last name
var splitFirstName = function(fullName) {
	var firstName = fullName.split(' ').slice(0, -1).join(' ');
	return firstName;
};

var splitLastName = function(fullName) {
	var lastName = fullName.split(' ').slice(-1).join(' ');
	return lastName;
};

// Function to conver the full name into first and last name
var fillInput = function() {
	var fullName = document.getElementById("fullName").value;
	// console.log(fullName);

	// Add the value of the full name into the First Name and Last Name inputs
	var firstNameInput = document.getElementsByName("firstName")[0];
	var lastNameInput = document.getElementsByName("lastName")[0];

	// Set the value using Angular, adding * to indicate it is a test result
	angular.element(document.querySelector('#firstName'))[0].value = splitFirstName(fullName) + "*";
	angular.element(document.querySelector('#lastName'))[0].value = splitLastName(fullName) + "*";
 
 	// triggerKeyboardOnInput();
};

//////////////////// DEBUGGING CODE - not effective   
// Programatically trigger a keyboard space entry on the input for $('#firstName') so that it can be validated in the form submission
// var triggerKeyboardOnInput = function() {
// 	jQuery(function($) {
//     	$("#firstName").focus(); // focus on the firstName input

// 		var keyVal = 32; // KeyCode for spacebar is 32
//     	var e = jQuery.Event("keydown"); 
// 		e.which = keyVal; 
// 		e.charCode = keyVal;
// 		$("#firstName").trigger(e); // trigger the keydown event
// 		// $("#firstName").sendkeys ("B") ; // using the SendKeys API to trigger the keyboard action 
// 	});	
// };